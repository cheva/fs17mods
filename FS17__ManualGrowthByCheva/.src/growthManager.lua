growthManager = {};


addModEventListener(growthManager);

function growthManager:hourChanged()
	local hour = g_currentMission.environment.currentHour;
	if g_currentMission.environment.currentHour == 0 then
		hour = 24;
	end;
	local allHours = (g_currentMission.environment.currentDay-1)*24 + hour;
	-- print("hours since game start: "..tostring(allHours))
	for index,gHours in pairs(growthManager.fruitList) do
		if (allHours-1)%gHours == 0 then --its time.
			table.insert(growthManager.fruitListWaiting,index);
			-- print("growthManager:: fruit scheduled for growth: "..FruitUtil.fruitIndexToDesc[index].name)
		end;
	end;
	
	if not growthManager.do24h then
		growthManager:dayChanged();
	end;
end;

function growthManager:dayChanged()
	if not growthManager.isActiveScanning then
		if #growthManager.fruitListWaiting > 0 then
			-- local backupDoubles = {};
			local numActiveFruits = 0;
			while #growthManager.fruitListWaiting>0 do
				local index = growthManager.fruitListWaiting[#growthManager.fruitListWaiting];

				-- if Utils.getNoNil(growthManager.fruitListActive[index],0)>0 then
					-- table.insert(backupDoubles,growthManager.fruitListWaiting[#growthManager.fruitListWaiting]);
					-- table.remove(growthManager.fruitListWaiting)
				-- else
					table.remove(growthManager.fruitListWaiting)
					-- growthManager.fruitListActive[index] = true;
					growthManager.fruitListActive[index] = Utils.getNoNil(growthManager.fruitListActive[index],0) + 1;
					if growthManager.fruitListActive[index] == 1 then
						numActiveFruits = numActiveFruits + 1;
						-- print("growthManager:: setting fruit active: "..FruitUtil.fruitIndexToDesc[index].name)
					else
						-- print("growthManager:: multiple growth: "..FruitUtil.fruitIndexToDesc[index].name)
					end;
				-- end;
			end;
			local numFruitLines = math.ceil(numActiveFruits / self.numFruitsPerLine);
			local bgHeight = (numFruitLines + 2) * (self.fruitOverlayData.h + self.fruitOverlayData.marginY); -- + 2 because of title and progressBar
			-- print(('numFruitLines=%d -> bgHeight=%f'):format(numFruitLines, bgHeight));
			self.infoBgOverlayId:setPosition(self.infoBgOverlayId.x, self.infoBgOverlayId.y + self.infoBgOverlayId.height - bgHeight);
			self.infoBgOverlayId:setDimension(self.infoBgOverlayId.width, bgHeight);

			growthManager.isActiveScanning = true;
			-- for _,fruit in pairs(backupDoubles) do
				-- table.insert(growthManager.fruitListWaiting,fruit)
			-- end;
		end;
	end;
end;



function growthManager:loadMap(name)

	print("--- loading growthManager mod V2.0 --- (by upsidedown+JakobT)")

	--this we make new for every map:
	growthManager.xVec = {};
	growthManager.zVec = {};
	growthManager.mapInitDone = false;
	growthManager.maxN = 0;
	growthManager.xCnt = 0;
	growthManager.zCnt = 1;
	growthManager.growthFactor = 1.0;
	growthManager.do24h = true;
	
	
	-- growthManager.stepSize = 250; -- edge length of scanning box
	growthManager.stepSize = 125; -- edge length of scanning box

	growthManager.timeCnt = 0;
	growthManager.isActiveScanning = false;

	-- g_currentMission.missionPDA:addStatsPage(self.drawStatsPageGrowthManager, self, nil, 2);
	g_currentMission.environment:addDayChangeListener(growthManager)
	g_currentMission.environment:addHourChangeListener(growthManager)

	growthManager.fruitListWaiting = {};
	growthManager.fruitListActive = {};

	local savegameIndex = g_currentMission.missionInfo.savegameIndex;
	local savegameFolderPath = g_currentMission.missionInfo.savegameDirectory;
	if savegameFolderPath == nil then
		savegameFolderPath = ('%ssavegame%d'):format(getUserProfileAppPath(), savegameIndex);
	end;


	if fileExists(savegameFolderPath .. '/careerSavegame.xml') then -- savegame isn't corrupted and has been saved correctly
		local key = "growthManager";
		if fileExists(savegameFolderPath .. '/growthManager.xml') then
			print("growthManager:: loading "..savegameFolderPath .. '/growthManager.xml')
			local xmlFile = loadXMLFile("growthManager", savegameFolderPath .. "/growthManager.xml", key);
			local isScan = getXMLBool(xmlFile, key.."#isActiveScanning");
			growthManager.isActiveScanning = isScan;
			
			growthManager.growthFactor = getXMLFloat(xmlFile, key.."#growthFactor") or 1;
			if math.abs(growthManager.growthFactor-1) > 0.001 then
				print("growthManager:: growth times are set to "..tostring(math.floor(growthManager.growthFactor*100)).."% for this savegame. This can be changed in "..savegameFolderPath .. "/growthManager.xml")
			end;
			
			growthManager.do24h = Utils.getNoNil(getXMLBool(xmlFile, key.."#doMidnight"),true);
			if growthManager.do24h then
				print("growthManager:: growth is shifted to midnight")
			else
				print("growthManager:: growth can occur every full hour")			
			end;

			local waitingList = getXMLString(xmlFile, key.."#waitingList");
			if waitingList~= nil then
				local types = Utils.splitString(" ", waitingList);
				 for k,v in pairs(types) do
					local index = FruitUtil["FRUITTYPE_"..string.upper(v)];
					if index~= nil then
						table.insert(growthManager.fruitListWaiting,index)
					end;
				 end;
			end;

			if isScan then
				local growingList = getXMLString(xmlFile, key.."#growingList");
				if growingList~= nil then
					local types = Utils.splitString(" ", growingList);
					for k,v in pairs(types) do
						local index = FruitUtil["FRUITTYPE_"..string.upper(v)];
						if index~= nil then
							growthManager.fruitListActive[index] = Utils.getNoNil(growthManager.fruitListActive[index],0) + 1;
						end;
					end;
					growthManager.xCnt = getXMLInt(xmlFile, key.."#xCnt");
					growthManager.zCnt = getXMLInt(xmlFile, key.."#zCnt");
				end;
			end;

			--<growthManager isActiveScanning="true" waitingList="" growingList="luzerne weed grass klee " xCnt="33" zCnt="4" />
			--<growthManager isActiveScanning="false" waitingList="wheat barley rape maize potato sugarBeet dryGrass oat rye sunflower " />

			delete(xmlFile);
		end;
	end;

	-- GUI
	self.fruitOverlayData = {};
	self.fruitOverlayData.w = 48/1980;
	self.fruitOverlayData.h = self.fruitOverlayData.w * g_screenAspectRatio;
	self.fruitOverlayData.marginX = 10/1980;
	self.fruitOverlayData.marginY = self.fruitOverlayData.marginX * g_screenAspectRatio;
	local w = 300/1980;
	local h = w * g_screenAspectRatio;
	local x = 1 - w - self.fruitOverlayData.marginX;
	-- local y = g_currentMission.weatherTimeBackgroundOverlay.y - h - self.fruitOverlayData.marginY;
	local y = .5; --DEBUG
	self.infoBgOverlayId = Overlay:new('growthManagerBackground', 'dataS2/menu/white.png', x, y, w, h);
	self.infoBgOverlayId:setColor(0,0,0,0.75);
	self.fruitOverlayData.baseX = x + self.fruitOverlayData.marginX;
	self.fruitOverlayData.baseY = y + h - self.fruitOverlayData.h;
	self.fruitOverlayData.titleY = self.fruitOverlayData.baseY + self.fruitOverlayData.marginY;
	self.numFruitsPerLine = 5;
	self.progressBarOverlayId = createImageOverlay('dataS2/menu/white.png');
	self.progressBarHeight = 4/1980 * g_screenAspectRatio;
	self.progressBarMaxWidth = w - self.fruitOverlayData.marginX * 2;
	self.percentColors = {
		  [0] = { 225/255,  27/255, 0 },
		 [50] = { 255/255, 204/255, 0 },
		[100] = {   0/255, 199/255, 0 }
	};
	self.colorMapSteps = 50;
	self.fruitOverlays = {};
end;

function growthManager:deleteMap()
	growthManager.mapInitDone = false;
	-- g_currentMission.environment:removeDayChangeListener(growthManager)
	-- g_currentMission.environment:removeHourChangeListener(growthManager)

	for index, overlayId in pairs(self.fruitOverlays) do
		delete(overlayId);
	end;
	delete(self.progressBarOverlayId);
end


function growthManager:mouseEvent(posX, posY, isDown, isUp, button)
end;

function growthManager:keyEvent(unicode, sym, modifier, isDown)
end;

function growthManager:update(dt)

	if true then
		g_currentMission:setPlantGrowthRate(1,nil);
		g_currentMission:setPlantGrowthRateLocked(true);		
	end;


	if not growthManager.mapInitDone then
		if true then --init terrain scanner
			local tSize = g_currentMission.terrainSize;

			local stepSize = growthManager.stepSize;

			local pixelSize = g_currentMission.terrainSize / g_currentMission.terrainDetailMapSize;
			stepSize = math.floor(stepSize/pixelSize)*pixelSize;
			print("growthManager:: stepSize: "..tostring(stepSize))
			growthManager.usedStepSize = stepSize;
			growthManager.pixelSize = pixelSize;
			local cnt = 1;
			for value=-tSize*0.5,tSize*0.5,stepSize do
				growthManager.xVec[cnt] = value;
				growthManager.zVec[cnt] = value;
				cnt = cnt+1;
			end;
			growthManager.maxN = cnt-1;
		end;

		growthManager.fruitList = {};
		for index,fruit in pairs(g_currentMission.fruits) do

			-- local hours = fruit.originalGrowthStateTime/3.6e6;
			-- local hours = FruitUtil.fruitTypeGrowths.dryGrass.growthStateTime = 36000000 (number)/3.6e6;
			local desc = FruitUtil.fruitIndexToDesc[index];
			
			local hours = FruitUtil.fruitTypeGrowths[desc.name].growthStateTime/3.6e6;
			
			-- local hours = 24;
			
			hours = math.floor(hours*Utils.getNoNil(growthManager.growthFactor,1.0));
			hours = math.max(hours,1);

			growthManager.fruitList[index] = hours;

			if index == FruitUtil.FRUITTYPE_DRYGRASS then
				growthManager.fruitList[index] = nil;
			-- elseif index == FruitUtil.FRUITTYPE_MANURESOLID then
				-- growthManager.fruitList[index] = nil;
			-- elseif index == FruitUtil.FRUITTYPE_MANURELIQUID then
				-- growthManager.fruitList[index] = nil;
			-- elseif index == FruitUtil.FRUITTYPE_KALKSOLID then
				-- growthManager.fruitList[index] = nil;
			-- elseif index == FruitUtil.FRUITTYPE_CHOPPEDRAPE then
				-- growthManager.fruitList[index] = nil;
			-- elseif index == FruitUtil.FRUITTYPE_CHOPPEDMAIZE then
				-- growthManager.fruitList[index] = nil;
			-- elseif index == FruitUtil.FRUITTYPE_CHOPPEDSTRAW then
				-- growthManager.fruitList[index] = nil;
			end;

			if growthManager.fruitList[index]~= nil then
				local desc = FruitUtil.fruitIndexToDesc[index];
				print("growthManager:: registering fruit "..desc.name.." growthTime: "..tostring(hours).."h")

				-- GUI
				local fillType = FruitUtil.fruitTypeToFillType[index];
				local ovlFileNameSmall = FillUtil.fillTypeIndexToDesc[fillType].hudOverlayFilenameSmall;
				-- print(('[%s] self.fruitOverlays[%d] -> %q'):format(tostring(desc.name), index, tostring(ovlFileNameSmall)));
				if type(ovlFileNameSmall) == 'string' and (ovlFileNameSmall:sub(-4) == '.png' or ovlFileNameSmall:sub(-4) == '.dds') then -- need for type and extension checking, as old fruit registration scripts don't use correct values or parameter positions
					self.fruitOverlays[index] = createImageOverlay(ovlFileNameSmall);
					-- print(('     -> ovl=%s'):format(tostring(self.fruitOverlays[index])));
				end;
			end;
		end;

		g_currentMission.hasGrowthManager = true;
		growthManager.mapInitDone = true;
	end;-- init done


	if growthManager.isActiveScanning then
		growthManager.timeCnt = growthManager.timeCnt + dt;

		growthManager.xCnt = growthManager.xCnt + 1;
		if growthManager.xCnt > growthManager.maxN then
			growthManager.xCnt = 1;
			growthManager.zCnt = growthManager.zCnt + 1;
			if growthManager.zCnt > growthManager.maxN then
				growthManager.zCnt = 1;
				growthManager.isActiveScanning = false;
				growthManager.fruitListActive = {};

				-- print("growthManager:: finished growth cycle!")

			elseif true then --debug print after each full row:
				-- print("growthManager ETC: ",growthManager.timeCnt*0.001*growthManager.maxN/60," Minuten: ",growthManager.zCnt/growthManager.maxN*100," Prozent")
			end;
			growthManager.timeCnt = 0;
		end;

		if true then
			local startWorldX = growthManager.xVec[growthManager.xCnt];
			local startWorldZ = growthManager.zVec[growthManager.zCnt];
			local widthWorldX = startWorldX + growthManager.usedStepSize-growthManager.pixelSize;
			local widthWorldZ = startWorldZ;
			local heightWorldX = startWorldX;
			local heightWorldZ = startWorldZ + growthManager.usedStepSize-growthManager.pixelSize;

			local detailId = g_currentMission.terrainDetailId;
						
			for index,fruit in pairs(g_currentMission.fruits) do
				if Utils.getNoNil(growthManager.fruitListActive[index],0)>0 and fruit.id > 0 then
					-- print("index: "..tostring(index))
					local x,z, widthX,widthZ, heightX,heightZ = Utils.getXZWidthAndHeight(id, startWorldX, startWorldZ, widthWorldX, widthWorldZ, heightWorldX, heightWorldZ);

					
					-- if true then -- do simple growth, thank you
					-- while Utils.getNoNil(growthManager.fruitListActive[index],0) > 0 do
					for mg=1, growthManager.fruitListActive[index] do
						local desc = FruitUtil.fruitIndexToDesc[index];
						--local maxState = desc.maxHarvestingGrowthState;
						--if desc.minPreparingGrowthState > 0 then
							-- maxState = desc.maxPreparingGrowthState;
						-- end;
						local growthData = FruitUtil.fruitTypeGrowths[desc.name];
						local maxState = growthData.numGrowthStates;
						if g_currentMission.loadingScreen.missionInfo.isPlantWitheringEnabled then
							maxState = growthData.witheringNumGrowthStates;
						end;
						maxState = maxState -1;
						
						setDensityMaskParams(fruit.id, "between", 1,maxState)
						-- local sum = addDensityMaskedParallelogram(fruit.id,x,z, widthX,widthZ, heightX,heightZ,0, g_currentMission.numFruitStateChannels, fruit.id, 0, g_currentMission.numFruitStateChannels, 1)
						local sum = addDensityMaskedParallelogram(fruit.id,x,z, widthX,widthZ, heightX,heightZ,0, g_currentMission.numFruitStateChannels, fruit.id, 0, g_currentMission.numFruitStateChannels, 1)
						
						
						if sum == 0 then
							break;
						else
							if growthData.resetsSpray then
								local sum2 = addDensityMaskedParallelogram(detailId,x,z, widthX,widthZ, heightX,heightZ, g_currentMission.sprayFirstChannel, g_currentMission.sprayNumChannels, fruit.id, 0, g_currentMission.numFruitStateChannels, -1)
							end;
							if growthData.groundTypeChanged > 0 then
								setDensityCompareParams(detailId, "greater", 0);
								local sum2 = setDensityMaskedParallelogram(detailId,x,z, widthX,widthZ, heightX,heightZ, g_currentMission.terrainDetailTypeFirstChannel, g_currentMission.terrainDetailTypeNumChannels, fruit.id, growthData.groundTypeChangeGrowthState, g_currentMission.numFruitStateChannels, growthData.groundTypeChanged);
								setDensityCompareParams(detailId, "greater", -1);
							end;
						end;
						-- growthManager.fruitListActive[index] = growthManager.fruitListActive[index] - 1;    
					end;					

				end;
			end;



-- FROM groundResponse:
-- setDensityCompareParams(detailId, "greater", 0);
-- local density, area, _ = getDensityParallelogram(detailId, x,z, widthX,widthZ, heightX,heightZ, g_currentMission.terrainDetailTypeFirstChannel, g_currentMission.terrainDetailTypeNumChannels);
-- setDensityCompareParams(detailId, "greater", -1);

-- local terrainValue = 0;
-- if area > 0 then
	-- terrainValue = math.floor(density/area + 0.5);
-- end
-- local minVal, maxVal = 0, 1;
-- local sinkSpeed = (dt/1000) * (speed/10) * (0.01 * math.random(10, 200));
-- local maxSink = math.min(0.1, wheel.radius*0.2 * (0.7 + 0.5*g_currentMission.environment.groundWetness));

-- if terrainValue == 1 then           -- cultivator
	-- minVal, maxVal = 20, 60;
	-- sinkSpeed = sinkSpeed * 0.9;

-- elseif terrainValue == 2 then       -- plough
	-- minVal, maxVal = 40, 100;
	-- sinkSpeed = sinkSpeed * 0.7;

-- elseif terrainValue == 3 then       -- sowing
	-- minVal, maxVal = 0, 30;
	-- sinkSpeed = sinkSpeed * 1.0;

-- elseif terrainValue == 4 then       -- sowingWidth
	-- minVal, maxVal = 30, 80;
	-- sinkSpeed = sinkSpeed * 0.8;

-- elseif terrainValue == 5 then       -- grass
	-- minVal, maxVal = 5, 30;
	-- sinkSpeed = sinkSpeed * 0.6;

-- end
			



			
-- FruitUtil.fruitTypeGrowths.soybean.witheringNumGrowthStates = 8 (number)
-- FruitUtil.fruitTypeGrowths.soybean.numGrowthStates = 7 (number)			
-- FruitUtil.fruitTypeGrowths.soybean.resetsSpray = true (boolean)
			
			------------------------------------------
-- addDensityMaskedParallelogram()
	-- name="(*temporary)", value=6509 (type "number")
	-- name="(*temporary)", value=164.88441467285 (type "number")
	-- name="(*temporary)", value=9.0197658538818 (type "number")
	-- name="(*temporary)", value=-4.3862609863281 (type "number")
	-- name="(*temporary)", value=-0.044708251953125 (type "number")
	-- name="(*temporary)", value=0.008697509765625 (type "number")
	-- name="(*temporary)", value=-0.6967134475708 (type "number")
	-- name="(*temporary)", value=9 (type "number")
	-- name="(*temporary)", value=2 (type "number")
	-- name="(*temporary)", value=6495 (type "number")
	-- name="(*temporary)", value=0 (type "number")
	-- name="(*temporary)", value=4 (type "number")
	-- name="(*temporary)", value=-1 (type "number")
----------------------------------------
-- addDensityMaskedParallelogram()
	-- name="(*temporary)", value=6509 (type "number")
	-- name="(*temporary)", value=164.88441467285 (type "number")
	-- name="(*temporary)", value=9.0197658538818 (type "number")
	-- name="(*temporary)", value=-4.3862609863281 (type "number")
	-- name="(*temporary)", value=-0.044708251953125 (type "number")
	-- name="(*temporary)", value=0.008697509765625 (type "number")
	-- name="(*temporary)", value=-0.6967134475708 (type "number")
	-- name="(*temporary)", value=9 (type "number")
	-- name="(*temporary)", value=2 (type "number")
	-- name="(*temporary)", value=6504 (type "number")
	-- name="(*temporary)", value=0 (type "number")
	-- name="(*temporary)", value=4 (type "number")
	-- name="(*temporary)", value=-1 (type "number")
----------------------------------------
-- addDensityMaskedParallelogram()
	-- name="(*temporary)", value=6509 (type "number")
	-- name="(*temporary)", value=164.88360595703 (type "number")
	-- name="(*temporary)", value=9.1013097763062 (type "number")
	-- name="(*temporary)", value=-4.3862457275391 (type "number")
	-- name="(*temporary)", value=-0.044163703918457 (type "number")
	-- name="(*temporary)", value=0.0086517333984375 (type "number")
	-- name="(*temporary)", value=-0.69665622711182 (type "number")
	-- name="(*temporary)", value=9 (type "number")
	-- name="(*temporary)", value=2 (type "number")
	-- name="(*temporary)", value=6493 (type "number")
	-- name="(*temporary)", value=0 (type "number")
	-- name="(*temporary)", value=4 (type "number")
	-- name="(*temporary)", value=-1 (type "number")
----------------------------------------
-- addDensityMaskedParallelogram()
	-- name="(*temporary)", value=6509 (type "number")
	-- name="(*temporary)", value=164.88360595703 (type "number")
	-- name="(*temporary)", value=9.1013097763062 (type "number")
	-- name="(*temporary)", value=-4.3862457275391 (type "number")
	-- name="(*temporary)", value=-0.044163703918457 (type "number")
	-- name="(*temporary)", value=0.0086517333984375 (type "number")
	-- name="(*temporary)", value=-0.69665622711182 (type "number")
	-- name="(*temporary)", value=9 (type "number")
	-- name="(*temporary)", value=2 (type "number")
	-- name="(*temporary)", value=6496 (type "number")
	-- name="(*temporary)", value=0 (type "number")
	-- name="(*temporary)", value=4 (type "number")
	-- name="(*temporary)", value=-1 (type "number")
----------------------------------------
-- addDensityMaskedParallelogram()
	-- name="(*temporary)", value=6509 (type "number")
	-- name="(*temporary)", value=164.88360595703 (type "number")
	-- name="(*temporary)", value=9.1013097763062 (type "number")
	-- name="(*temporary)", value=-4.3862457275391 (type "number")
	-- name="(*temporary)", value=-0.044163703918457 (type "number")
	-- name="(*temporary)", value=0.0086517333984375 (type "number")
	-- name="(*temporary)", value=-0.69665622711182 (type "number")
	-- name="(*temporary)", value=9 (type "number")
	-- name="(*temporary)", value=2 (type "number")
	-- name="(*temporary)", value=6495 (type "number")
	-- name="(*temporary)", value=0 (type "number")
	-- name="(*temporary)", value=4 (type "number")
	-- name="(*temporary)", value=-1 (type "number")
----------------------------------------
-- addDensityMaskedParallelogram()
	-- name="(*temporary)", value=6509 (type "number")
	-- name="(*temporary)", value=164.88360595703 (type "number")
	-- name="(*temporary)", value=9.1013097763062 (type "number")
	-- name="(*temporary)", value=-4.3862457275391 (type "number")
	-- name="(*temporary)", value=-0.044163703918457 (type "number")
	-- name="(*temporary)", value=0.0086517333984375 (type "number")
	-- name="(*temporary)", value=-0.69665622711182 (type "number")
	-- name="(*temporary)", value=9 (type "number")
	-- name="(*temporary)", value=2 (type "number")
	-- name="(*temporary)", value=6504 (type "number")
	-- name="(*temporary)", value=0 (type "number")
	-- name="(*temporary)", value=4 (type "number")
	-- name="(*temporary)", value=-1 (type "number")


			

			
		end;
	end;
end;



function growthManager:draw()
	if g_currentMission.isPaused or g_gui.currentGui ~= nil or not growthManager.isActiveScanning then
		return;
	end;

	setTextColor(1,1,1,1);

	-- background
	self.infoBgOverlayId:render();

	-- fruit overlays
	local i, w, h = 0, self.fruitOverlayData.w, self.fruitOverlayData.h;
	local col, line, x, y;
	for fruitIndex,growing in pairs(growthManager.fruitListActive) do
		if Utils.getNoNil(growing,0)>0 and self.fruitOverlays[fruitIndex] then
			col = i % self.numFruitsPerLine;
			line = math.floor(i / self.numFruitsPerLine) + 1;
			x = self.fruitOverlayData.baseX + col * (w + self.fruitOverlayData.marginX);
			y = self.fruitOverlayData.baseY - line * (h + self.fruitOverlayData.marginY);

			renderOverlay(self.fruitOverlays[fruitIndex], x, y, w, h);

			i = i + 1;
		end;
	end;

	-- progress bar
	if y then
		y = y - (h + self.fruitOverlayData.marginY);

		setOverlayColor(self.progressBarOverlayId, 0.2,0.2,0.2, 0.9);
		renderOverlay(self.progressBarOverlayId, self.fruitOverlayData.baseX, y, self.progressBarMaxWidth, self.progressBarHeight);

		local pct = (growthManager.zCnt-1 + growthManager.xCnt / growthManager.maxN) / growthManager.maxN;
		local r, g, b = self:getColorFromPct(pct * 100, self.percentColors, self.colorMapSteps);
		setOverlayColor(self.progressBarOverlayId, r,g,b, 1);
		renderOverlay(self.progressBarOverlayId, self.fruitOverlayData.baseX, y, self.progressBarMaxWidth * pct, self.progressBarHeight);

		setTextAlignment(RenderText.ALIGN_CENTER);
		local textX = self.fruitOverlayData.baseX + self.progressBarMaxWidth * pct;
		renderText(textX, y + self.progressBarHeight * 2, 0.014, ('%.1f%%'):format(pct * 100));
	end;

	-- title
	setTextAlignment(RenderText.ALIGN_LEFT);
	setTextBold(true);
	renderText(self.fruitOverlayData.baseX, self.fruitOverlayData.titleY, 0.024, g_i18n:getText('GROWTHMANAGER_FRUITGROWTH'));
	setTextBold(false);
end;

function growthManager:getColorFromPct(pct, colorMap, step)
	if colorMap[pct] then
		return unpack(colorMap[pct]);
	end;

	local lower = math.floor(pct / step) * step;
	local upper = math.ceil(pct / step) * step;

	local alpha = (pct - lower) / step;
	return Utils.vector3ArrayLerp(colorMap[lower], colorMap[upper], alpha);
end;




--DEBUG CHECK!!!
-- function FSBaseMission:updateFoliageGrowthStateTime()
  -- local multiplier = 1024;
  -- for _, entry in pairs(self.fruits) do
    -- if entry.id ~= 0 then
      -- setGrowthStateTime(entry.id, entry.originalGrowthStateTime * multiplier)
      -- setGrowthStateTime(entry.id, 10000000 * multiplier)
    -- end
  -- end
-- end


--DEBUG CHECK!!!
-- function FSBaseMission:getFoliageGrowthStateTimeMultiplier()
	-- local value = math.huge;
	-- return value;
-- end;


function growthManager:saveGrowthState()
	local savegameIndex = g_currentMission.missionInfo.savegameIndex;
	local savegameFolderPath = g_currentMission.missionInfo.savegameDirectory;
	if savegameFolderPath == nil then
		savegameFolderPath = ('%ssavegame%d'):format(getUserProfileAppPath(), savegameIndex);
	end;

	if fileExists(savegameFolderPath .. '/careerSavegame.xml') then -- savegame isn't corrupted and has been saved correctly
		local key = "growthManager";
		print("growthManager:: Saving to "..savegameFolderPath .. "/growthManager.xml")
		local xmlFile = createXMLFile("growthManager", savegameFolderPath .. "/growthManager.xml", key);
		setXMLBool(xmlFile, key .. "#isActiveScanning",growthManager.isActiveScanning);

		local str = "";
		for _,index in pairs(growthManager.fruitListWaiting) do
			local desc = FruitUtil.fruitIndexToDesc[index];
			str = str..desc.name.." ";
		end;
		setXMLString(xmlFile, key .. "#waitingList",str);

		if growthManager.isActiveScanning then
			local str2 = "";
			for index,growing in pairs(growthManager.fruitListActive) do
				-- if growing then
					-- local desc = FruitUtil.fruitIndexToDesc[index];
					-- str2 = str2..desc.name.." ";
				-- end;
				if Utils.getNoNil(growing,0)>0 then
					for k=1,growing do
						local desc = FruitUtil.fruitIndexToDesc[index];
						str2 = str2..desc.name.." ";
					end;
				end;
			end;
			setXMLString(xmlFile, key .. "#growingList",str2);

			setXMLInt(xmlFile, key .. "#xCnt",growthManager.xCnt);
			setXMLInt(xmlFile, key .. "#zCnt",growthManager.zCnt);
		end;
		
		setXMLFloat(xmlFile, key .. "#growthFactor",growthManager.growthFactor);
		setXMLBool(xmlFile, key .. "#doMidnight",growthManager.do24h);

		saveXMLFile(xmlFile);
		delete(xmlFile);
	end;
end;

g_careerScreen.saveSavegame = Utils.appendedFunction(g_careerScreen.saveSavegame, growthManager.saveGrowthState);




-- ################################################################################
-- MULTIPLAYER

local origServerSendObjects = Server.sendObjects;
function Server:sendObjects(connection, x, y, z, viewDistanceCoeff)
	connection:sendEvent(GMjoinEvent:new());
	return origServerSendObjects(self, connection, x, y, z, viewDistanceCoeff);
end

GMjoinEvent = {};
GMjoinEvent_mt = Class(GMjoinEvent, Event);
InitEventClass(GMjoinEvent, 'GMjoinEvent');

function GMjoinEvent:emptyNew()
	local self = Event:new(GMjoinEvent_mt);
	self.className = 'growthManager.GMjoinEvent';
	return self;
end
function GMjoinEvent:new()
	local self = GMjoinEvent:emptyNew()
	return self;
end

function GMjoinEvent:writeStream(streamId, connection)
	if not connection:getIsServer() then
		-- print("sending data to joining client:")
		-- print(tostring(growthManager))
		-- print(growthManager.isActiveScanning)
		
		local str = "";
		for _,index in pairs(growthManager.fruitListWaiting) do
			local desc = FruitUtil.fruitIndexToDesc[index];
			str = str..desc.name.." ";
		end;
		streamWriteString(streamId, str);
		-- setXMLString(xmlFile, key .. "#waitingList",str);

		streamWriteBool(streamId, growthManager.isActiveScanning);
		streamWriteFloat32(streamId, growthManager.growthFactor);
		streamWriteBool(streamId, growthManager.do24h);
		if growthManager.isActiveScanning then
			local str2 = "";
			for index,growing in pairs(growthManager.fruitListActive) do
				-- if growing then
					-- local desc = FruitUtil.fruitIndexToDesc[index];
					-- str2 = str2..desc.name.." ";
				-- end;
				if Utils.getNoNil(growing,0)>0 then
					for k=1,growing do
						local desc = FruitUtil.fruitIndexToDesc[index];
						str2 = str2..desc.name.." ";
					end;
				end;
			end;
			streamWriteString(streamId, str2);
			streamWriteInt32(streamId, growthManager.xCnt);
			streamWriteInt32(streamId, growthManager.zCnt);
		end;
	end;
end;
function GMjoinEvent:readStream(streamId, connection)
	if connection:getIsServer() then
		local waitingList = streamReadString(streamId);
		if waitingList~= nil then
			local types = Utils.splitString(" ", waitingList);
			 for k,v in pairs(types) do
				local index = FruitUtil["FRUITTYPE_"..string.upper(v)];
				if index~= nil then
					table.insert(growthManager.fruitListWaiting,index)
				end;
			 end;
		end;

		local isScan = streamReadBool(streamId);
		growthManager.isActiveScanning = isScan;
		growthManager.growthFactor = streamReadFloat32(streamId);		
		growthManager.do24h = streamReadBool(streamId);
		
		if isScan then
			local growingList = streamReadString(streamId);
			if growingList~= nil then
				local types = Utils.splitString(" ", growingList);
				for k,v in pairs(types) do
					local index = FruitUtil["FRUITTYPE_"..string.upper(v)];
					if index~= nil then
						growthManager.fruitListActive[index] = Utils.getNoNil(growthManager.fruitListActive[index],0)+1;
					end;
				end;
				growthManager.xCnt = streamReadInt32(streamId);
				growthManager.zCnt = streamReadInt32(streamId);
			end;
		end;
	end;
end;


-- ################################################################################




--
-- growthcontrol
-- V17.2
--
-- @author upsideDown+JakobT/apuehri/Cheva
-- @date   28/11/2016
--
-- Copyright (C) apuehri
-- V17.0 ..... first implementation
-- V17.1 ..... 04/12/2016 recalculate after saving
-- V17.2 ..... 10/12/2016 optimized hud (new position,background,scalable), 
-- 							  modDesc.xml without entries for fruitnames
--								  changed standardvalues for growthtime
--								  it's now possible to set values for literPerSqm, seedUsagePerSqm
-- V17.3 ..... 27/12/2016 remaining hours in different colors 
--								  changed Modname to FS17_GrowthControl

growthcontrol = {};
growthcontrol.Version = 17.3;

function growthcontrol.prerequisitesPresent(specializations)
    return true;
end;

function growthcontrol:loadMap(name)
	--initialize
	growthcontrol.showHud = false;
	growthcontrol.recalculate = false;
	growthcontrol.maxnumfruits = 11;
	growthcontrol.fruitnames = {[0]="wheat",[1]="grass",[2]="rape",[3]="barley",[4]="maize",[5]="not used",[6]="potato",[7]="sugarBeet",[8]="sunflower",[9]="soybean",[10]="oilseedRadish",[11]="poplar"};
	growthcontrol.growthhours = {[0]=30,[1]=22,[2]=25,[3]=28.5,[4]=21,[5]=0,[6]=15,[7]=18.5,[8]=19,[9]=18,[10]=20,[11]=35};	
	growthcontrol.growthstatetime = {};
	growthcontrol.growthremtime = {};
	growthcontrol.literPerSqm = {};
	growthcontrol.seedUsagePerSqm = {};
	
	-- Hud
	local uiScale = Utils.getUIScaleFromIndex(g_settingsScreen.defaultSettings.uiScale.current);

	growthcontrol.tPos = {};
	growthcontrol.tPos.size = 0.015 * uiScale;				-- TextSize
	growthcontrol.tPos.spacing = 0.003 * uiScale;			-- Spacing
	growthcontrol.tPos.alignment = RenderText.ALIGN_LEFT;	-- Text Alignment
	growthcontrol.tPos.bgoffset = 0.002 * uiScale; 			-- Offset to Background
	growthcontrol.tPos.yoffset = 0.006 * uiScale ; 			-- Offset y-Startpos
	growthcontrol.tPos.x = 0.61 + (0.3 * (1 - uiScale))   -- x Pos
	growthcontrol.tPos.y = g_currentMission.weatherForecastBgOverlay.y + g_currentMission.weatherForecastBgOverlay.height - growthcontrol.tPos.size + growthcontrol.tPos.yoffset -- y Pos	
	growthcontrol.tPos.h = ((growthcontrol.tPos.size+growthcontrol.tPos.spacing)*growthcontrol.maxnumfruits) + growthcontrol.tPos.size + (2*growthcontrol.tPos.bgoffset); -- height
	growthcontrol.tPos.w = 0.15 * uiScale;	-- width

	local ypos = growthcontrol.tPos.y - growthcontrol.tPos.h + growthcontrol.tPos.size;
	growthcontrol.tPosBgOverlayId = Overlay:new('growthcontrolBackground', 'dataS2/menu/white.png', growthcontrol.tPos.x - growthcontrol.tPos.bgoffset, ypos , growthcontrol.tPos.w , growthcontrol.tPos.h );
	growthcontrol.tPosBgOverlayId:setColor(0,0,0,0.80); --0.75
	
	-- MinuteChangeListener
	g_currentMission.environment:addMinuteChangeListener(growthcontrol);
	
	--load Savegame
	local savegameIndex = g_currentMission.missionInfo.savegameIndex;
	local savegameFolderPath = g_currentMission.missionInfo.savegameDirectory;
	if savegameFolderPath == nil then
		savegameFolderPath = ('%ssavegame%d'):format(getUserProfileAppPath(), savegameIndex);
	end;

	if fileExists(savegameFolderPath .. '/careerSavegame.xml') then
		if fileExists(savegameFolderPath .. '/growthcontrol.xml') then
			print("--- loading growthcontrol V"..string.format("%1.1f",growthcontrol.Version).." (c) by aPuehri --- loading savegame ---");
			local key = "growthcontrol";
			local xmlFile = loadXMLFile("growthcontrol", savegameFolderPath .. "/growthcontrol.xml", key);
				if xmlFile ~= nil then
					for i=0, growthcontrol.maxnumfruits do
						growthcontrol.growthhours[i] = getXMLFloat(xmlFile, "growthcontrol.fruit("..i..")#hours");
						growthcontrol.growthstatetime[i] = growthcontrol.growthhours[i]*3.6e6;
						growthcontrol.literPerSqm[i] = getXMLFloat(xmlFile, "growthcontrol.fruit("..i..")#literPerSqm");
						growthcontrol.seedUsagePerSqm[i] = getXMLFloat(xmlFile, "growthcontrol.fruit("..i..")#seedUsagePerSqm");
						local frname = growthcontrol.fruitnames[i];
						if (frname ~= nil) and (frname ~= "not used") then
							FruitUtil.fruitTypeGrowths[frname].growthStateTime = growthcontrol.growthstatetime[i];
							local desc = FruitUtil.fruitIndexToDesc[FruitUtil.fruitTypes[growthcontrol.fruitnames[i]].index];
							if (growthcontrol.literPerSqm[i] ~= nil) and (growthcontrol.literPerSqm[i] >= 0.1) then
								desc.literPerSqm = growthcontrol.literPerSqm[i];
							else
								growthcontrol.literPerSqm[i] = desc.literPerSqm;
							end;
							if (growthcontrol.seedUsagePerSqm[i] ~= nil) and (growthcontrol.seedUsagePerSqm[i] >= 0.001) then
								desc.seedUsagePerSqm = growthcontrol.seedUsagePerSqm[i];
							else
								growthcontrol.seedUsagePerSqm[i] = desc.seedUsagePerSqm;
							end;
						end;
					end;				
				end;
			delete(xmlFile);
		else
			print("--- loading growthcontrol V"..string.format("%1.1f",growthcontrol.Version).." (c) by aPuehri --- loading initialvalues ---");
			for i=0, growthcontrol.maxnumfruits do
				growthcontrol.growthstatetime[i] = growthcontrol.growthhours[i]*3.6e6;
				local frname = growthcontrol.fruitnames[i];
				if (frname ~= nil) and (frname ~= "not used") then
					FruitUtil.fruitTypeGrowths[frname].growthStateTime = growthcontrol.growthstatetime[i];
					local desc = FruitUtil.fruitIndexToDesc[FruitUtil.fruitTypes[growthcontrol.fruitnames[i]].index];
					growthcontrol.literPerSqm[i] = desc.literPerSqm;
					growthcontrol.seedUsagePerSqm[i] = desc.seedUsagePerSqm;
				end;
			end;			
		end;

		--read values of fruit_density_growthState.xml and calculate remaining growthtime in minutes
		if fileExists(savegameFolderPath .. '/fruit_density_growthState.xml') then
			local key = "foliageCropsUpdater";
			local xmlFile = loadXMLFile("fruit_density_growthState", savegameFolderPath .. "/fruit_density_growthState.xml", key);
			for i=0, growthcontrol.maxnumfruits do
				growthcontrol.growthremtime[i] = getXMLFloat(xmlFile, "foliageCropsUpdater.cropsState("..i..")#normalizedGrowthTimer");				
				if growthcontrol.growthremtime[i] == nil then
					growthcontrol.growthremtime[i] = 1;
				end;
				growthcontrol.growthremtime[i] = (growthcontrol.growthremtime[i] * growthcontrol.growthstatetime[i]) / 60000;
				print("---     Remaining Time["..growthcontrol.fruitnames[i].."]: = "..(growthcontrol.growthremtime[i]/60).." hours ---");
			end;
			delete(xmlFile);						
		end;		
	else
		print("--- loading growthcontrol V"..string.format("%1.1f",growthcontrol.Version).." (c) by aPuehri --- loading initialvalues ---");
		for i=0, growthcontrol.maxnumfruits do
			growthcontrol.growthstatetime[i] = growthcontrol.growthhours[i]*3.6e6;
			local frname = growthcontrol.fruitnames[i];
			if (frname ~= nil) and (frname ~= "not used") then
				FruitUtil.fruitTypeGrowths[frname].growthStateTime = growthcontrol.growthstatetime[i];
				local desc = FruitUtil.fruitIndexToDesc[FruitUtil.fruitTypes[growthcontrol.fruitnames[i]].index];
				growthcontrol.literPerSqm[i] = desc.literPerSqm;
				growthcontrol.seedUsagePerSqm[i] = desc.seedUsagePerSqm;			
			end;
			growthcontrol.growthremtime[i] = 1;			
			growthcontrol.growthremtime[i] = (growthcontrol.growthremtime[i] * growthcontrol.growthstatetime[i]) / 60000;
			print("---     Remaining Time["..growthcontrol.fruitnames[i].."]: = "..(growthcontrol.growthremtime[i]/60).." hours ---");			
		end;		
	end;
		
end;

function growthcontrol:deleteMap()
	growthcontrol.showHud = false;
	growthcontrol.recalculate = false;
end

function growthcontrol:mouseEvent(posX, posY, isDown, isUp, button)
end;

function growthcontrol:keyEvent(unicode, sym, modifier, isDown)
end;

function growthcontrol:update(dt)
	g_currentMission:setPlantGrowthRate(3,nil); --growthRate to normal
	g_currentMission:setPlantGrowthRateLocked(true); --lock growthRate Selection
	
	--showHud
	if InputBinding.hasEvent(InputBinding.gc_ToggleHud) then
		if not g_currentMission.isPaused and not growthcontrol.showHud then
			growthcontrol.showHud = true;
		elseif growthcontrol.showHud then
			growthcontrol.showHud = false;
		end;
	end;
	
	--check recalculation
	if not g_currentMission.isSaving and growthcontrol.recalculate then
		--print("--- growthcontrol initialized recalculation ---");
		--load Savegame
		local savegameIndex = g_currentMission.missionInfo.savegameIndex;
		local savegameFolderPath = g_currentMission.missionInfo.savegameDirectory;
		if savegameFolderPath == nil then
			savegameFolderPath = ('%ssavegame%d'):format(getUserProfileAppPath(), savegameIndex);
		end;

		if fileExists(savegameFolderPath .. '/careerSavegame.xml') then
			--read values of fruit_density_growthState.xml and calculate remaining growthtime in minutes
			if fileExists(savegameFolderPath .. '/fruit_density_growthState.xml') then
				local key = "foliageCropsUpdater";
				local xmlFile = loadXMLFile("fruit_density_growthState", savegameFolderPath .. "/fruit_density_growthState.xml", key);
				for i=0, growthcontrol.maxnumfruits do
					growthcontrol.growthremtime[i] = getXMLFloat(xmlFile, "foliageCropsUpdater.cropsState("..i..")#normalizedGrowthTimer");				
					if growthcontrol.growthremtime[i] == nil then
						growthcontrol.growthremtime[i] = 1;
					end;
					growthcontrol.growthremtime[i] = (growthcontrol.growthremtime[i] * growthcontrol.growthstatetime[i]) / 60000;
					--print("--- recalculated Remaining Time["..growthcontrol.fruitnames[i].."]: = "..(growthcontrol.growthremtime[i]/60).." hours ---");
				end;
				delete(xmlFile);
				growthcontrol.recalculate = false;
			end;		
		end;	
	end;
end

function growthcontrol:draw()
	if g_currentMission.isPaused or g_gui.currentGui ~= nil or not growthcontrol.showHud then
		return;
	end;
		
	setTextColor(1,1,1,1);
	
	growthcontrol.tPosBgOverlayId:render();	

	setTextAlignment(growthcontrol.tPos.alignment);
	setTextBold(true);
	renderText(growthcontrol.tPos.x, growthcontrol.tPos.y, growthcontrol.tPos.size , g_i18n:getText('growthstate'));
	setTextBold(false);
	local yPos = growthcontrol.tPos.y - growthcontrol.tPos.size - growthcontrol.tPos.spacing;
   for i=0, growthcontrol.maxnumfruits do
		if (growthcontrol.fruitnames[i] ~= nil) and (growthcontrol.fruitnames[i] ~= "not used") then
			local hudname = "..not def.";
			local desc = FillUtil.fillTypeIndexToDesc[FruitUtil.fruitTypes[growthcontrol.fruitnames[i]].index];
			if desc ~= nil then
				if g_i18n:hasText(desc.nameI18N) then
					hudname = g_i18n:getText(desc.nameI18N);
				else
					hudname = desc.nameI18N
				end;
			end;			
			if (growthcontrol.growthremtime[i]/60 <= 2.0) and (growthcontrol.growthremtime[i]/60 > 1.0) then
				setTextColor(0.82,0.41,0.11,1);
			elseif (growthcontrol.growthremtime[i]/60 <= 1.0) then
				setTextColor(1,0.27,0,1);
			else
				setTextColor(1,1,1,1);
			end;	
			renderText(growthcontrol.tPos.x, yPos, growthcontrol.tPos.size , tostring(hudname).." = "..(string.format("%.4f",growthcontrol.growthremtime[i]/60)).." "..g_i18n:getText('growthhour'));
			yPos = yPos - growthcontrol.tPos.size - growthcontrol.tPos.spacing;
		end;	
	end;	
end;

function growthcontrol:minuteChanged()
	for i=0, growthcontrol.maxnumfruits do
	   if (growthcontrol.growthremtime[i] > 0) then
			growthcontrol.growthremtime[i] = growthcontrol.growthremtime[i]-1;	
			--print("--- Restzeit Wachstum["..growthcontrol.fruitnames[i].."]: = "..(growthcontrol.growthremtime[i]/60).." Stunden ---");
		else
			growthcontrol.growthremtime[i] = growthcontrol.growthstatetime[i] / 60000;
		end;
	end;	
end;

function growthcontrol:saveSettings()
	local savegameIndex = g_currentMission.missionInfo.savegameIndex;
	local savegameFolderPath = g_currentMission.missionInfo.savegameDirectory;
	if savegameFolderPath == nil then
		savegameFolderPath = ('%ssavegame%d'):format(getUserProfileAppPath(), savegameIndex);
	end;
	
	if fileExists(savegameFolderPath .. '/careerSavegame.xml') then
		local key = "growthcontrol"; 
		local xmlFile = createXMLFile("growthcontrol", savegameFolderPath .. "/growthcontrol.xml", key);
			
		for i=0, growthcontrol.maxnumfruits do
			setXMLString(xmlFile, "growthcontrol.fruit("..i..")#name",growthcontrol.fruitnames[i]);
			setXMLFloat(xmlFile, "growthcontrol.fruit("..i..")#hours",growthcontrol.growthhours[i]);
			if (growthcontrol.fruitnames[i] ~= "not used") then
				setXMLFloat(xmlFile, "growthcontrol.fruit("..i..")#literPerSqm",growthcontrol.literPerSqm[i]);
				setXMLFloat(xmlFile, "growthcontrol.fruit("..i..")#seedUsagePerSqm",growthcontrol.seedUsagePerSqm[i]);
			end;
		end;
		saveXMLFile(xmlFile);
		delete(xmlFile);		
	end;
	growthcontrol.recalculate = true;
end;
g_careerScreen.saveSavegame = Utils.appendedFunction(g_careerScreen.saveSavegame, growthcontrol.saveSettings);

addModEventListener(growthcontrol);
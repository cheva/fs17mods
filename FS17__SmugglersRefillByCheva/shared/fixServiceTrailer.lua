local metadata = {
"## Interface:FS17 1.3.0.0 1.3RC4",
"## Title: fixServiceTrailer",
"## Notes: removed Trigge, hidde Warning: Deleting object ... before all triggers are removed.",
"## Author: Marhu",
"## Version: 1.0.0.1",
"## Date: 04.11.2016",
"## Web: http://marhu.net"
} 

fixServiceTrailer = {};

function fixServiceTrailer.prerequisitesPresent(specializations)
    return true;
end;

function fixServiceTrailer:load(xmlFile)
   self.fixTrigger = {};
   
   local baseKey = "vehicle.fixServiceTrailer";
   local i = 0
   while true do
      local key = string.format(baseKey.. ".ServiceTrigger(%d)", i);
      if not hasXMLProperty(self.xmlFile, key) then
         break;
      end;
      local trigger = Utils.indexToObject(self.components, getXMLString(self.xmlFile, key.. "#index"));
      if trigger ~= nil then
      --print("SericeTrigger: "..tostring(trigger))
         self.fixTrigger[trigger] = trigger;
      end
      i = i + 1;
   end
end;

function fixServiceTrailer:delete()
	for k,v in pairs(self.fixTrigger) do
		 removeTrigger(v);
	end
end;

function fixServiceTrailer:mouseEvent(posX, posY, isDown, isUp, button)
end;

function fixServiceTrailer:keyEvent(unicode, sym, modifier, isDown)
end;

function fixServiceTrailer:update(dt)
end;

function fixServiceTrailer:updateTick(dt)
end;

function fixServiceTrailer:draw()
end;